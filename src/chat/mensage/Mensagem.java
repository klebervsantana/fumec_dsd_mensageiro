package chat.mensage;

import javafx.beans.property.SimpleStringProperty;

/*
 * Atrav�s do metodo Mensagem, ele captura o autor e mensagem, transferindo os
 * valores para as vari�veis autor e mensagem.
 * Possibilitando reculperar estes valores atrav�s dos m�todos getAutor() e getMensagem()
*/

public class Mensagem {

	private final SimpleStringProperty autor, mensagem;

	public Mensagem(String autor, String mensagem) {
		this.autor = new SimpleStringProperty(autor);
		this.mensagem = new SimpleStringProperty(mensagem);
	}

	public String getAutor() {
		return autor.get();
	}

	public String getMensagem() {
		return mensagem.get();
	}
	
}
